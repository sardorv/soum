## Requirements

## Installation

## Install this project

Clone the project, move to the folder that you clonned and install the dependencies.
Before running the server make sure to generate sample data by running: `npm run mock`

```bash
git clone https://gitlab.com/sardorv/soum.git
cd soum
npm install
npm run mock
```

## Run server

In the server folder run the Rest API server with the command:

```bash
node run start
```

## Mock Data

Mock data can be generated separatly with following commands:

1. `npm run mock:category` Create categories
2. `npm run mock:products` Create products

By default script will generate 10 categories and 100 products

Using following command both categories and products can be generated

3. `npm run mock`

## Tests

Tests can be run by following command:

`npm run test`

## API Docs

`http://127.0.0.1:3000/docs`

## GraphQL API can be accessed at

`http://127.0.0.1:3000/graphql`

### Examples

#### REST

[This file](https://gitlab.com:sardorv/soum/Insomnia_2022-04-22.yaml) can be imported to insomnia

#### QraphQL

```bash
query getSingleUser($categoryID: String) {
  category(category_id: $categoryID) {
    category_id
    category_name
  }
}

query getListOfCategories($categoryID: String) {
  categories(parent_id:$categoryID) {
    category_id
    category_name
  }
}

query getSingleProduct($id: String) {
  product(id: $id) {
    id
    name
  }
}

query listProducts($state: String) {
  products(state: $state) {
    id
    name
  }
}

```
