#!/bin/bash

if [ $# -eq 1 ]; then
  env=$1
else
  env=dev
fi

if [ $env == "prod" ]
then
    region="us-east-1"
    ecr_url=844363451835.dkr.ecr.us-east-1.amazonaws.com/insa
elif [ $env == "dev" ]
then
    region="ap-northeast-2"
    ecr_url=844363451835.dkr.ecr.ap-northeast-2.amazonaws.com/insa
fi


aws ecr get-login-password \
        --profile 29labs \
        --region $region | docker login \
        --username AWS \
        --password-stdin $ecr_url

docker buildx build --platform=linux/amd64 --push -t "${ecr_url}:${env}" --build-arg NODE_ENV=$env .
