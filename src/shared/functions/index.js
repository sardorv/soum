const decodeSearch = search => decodeURI(search).replace(/\\/g, '\\\\');
module.exports = {
  decodeSearch,
};
