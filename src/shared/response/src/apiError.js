const errCodeList = require('./errCodeList');

const isTest = () => process.env.NODE_ENV === 'test';

class apiError extends Error {
  /**
   *
   * @param {number} [code=500] The HTTP status code to use
   * @param {string} [msgCode] The message code to send
   */
  constructor(code = 500, msgCode, { error, name, message, stack } = {}) {
    super();

    this.isApiError = true;

    // Set default code
    if (!code) {
      code = 500; // eslint-disable-line no-param-reassign
    }

    // Validate or set default msgCode
    if (msgCode) {
      msgCode = msgCode.toUpperCase(); // eslint-disable-line no-param-reassign
      msgCode = msgCode.split(' ').join('_'); // eslint-disable-line no-param-reassign
    } else {
      msgCode = errCodeList.codes.get(code) || 'UNKNOWN'; // eslint-disable-line no-param-reassign
    }

    this.name = name || msgCode;
    this.code = code;
    this.msgCode = msgCode;
    if (message) {
      this.message = message;
    }
    if (stack) {
      this.stack = stack;
    }
    this.errors = error || [];
    this.req = null;
    this.tags = {};

    return this;
  }

  /**
   * Set error value
   *
   * @param {Object} error Inforamtions related to the error
   */
  setErrors(errors) {
    this.errors = errors;
    if (errors.name) {
      this.name = errors.name;
    }
    this.message = errors.message;
  }

  /**
   * Set error stack trace
   *
   * @param {String} stack Inforamtions related to the error
   */
  setStack(stack) {
    this.stack = stack;
  }

  /**
   * Set req
   *
   * @param {*} req
   */
  setReq(req) {
    this.req = req;
  }

  /**
   * Set tag
   *
   * @param {*} tagName
   * @param {*} tagValue
   */
  setTag(tagName, tagValue) {
    this.tags[tagName] = tagValue;
  }

  /**
   * Determine if error come from server side
   */
  isServer() {
    return this.code >= 500;
  }

  /**
   * Send Error to front extends
   *
   * @param {*} res
   */
  send(res) {
    // const output = Object.assign({}, this);
    // delete output.code;
    // delete output.isApiError;
    // delete output.req;
    // delete output.name;
    // delete output.message;
    // delete output.tags;

    // // If Error is empty, we don t send it
    // if (Object.keys(output.error).length === 0) {
    // }
    // just dont send error always
    // delete output.error;
    const output = {
      // code: this.code,
      msgCode: this.msgCode,
    };
    if (this.msgCode === 'VALIDATION_ERROR' && this.errors) {
      output.errors = this.errors;
    }
    res.status(this.code).send(output); // eslint-disable-line no-restricted-properties
  }

  /**
   * Log error on sentry without having to send the error to front end
   */
  async log({ tags } = {}) {
    try {
      if (isTest() || !this.isServer()) {
        return;
      }

      if (!this.tags) {
        this.tags = {};
      }
      // merge tags
      if (tags) {
        Object.assign(this.tags, tags);
      }
      if (process && process.env && process.env.AWS_LAMBDA_FUNCTION_NAME) {
        this.tags.AWS_LAMBDA_FUNCTION_NAME =
          process.env.AWS_LAMBDA_FUNCTION_NAME;
      }

      // Sentry.init({
      //   dsn: 'https://21b5bc74c232468c9ac9dc465f163971@sentry.io/1327289',
      //   captureUnhandledRejections: true,
      //   allowDuplicates: true,
      //   environment: process.env.NODE_ENV,
      //   shutdownTimeout: 2
      // });

      // Sentry.configureScope((scope) => {
      //   scope.setUser({ id: this.req && this.req.body ? this.req.body.userId : undefined });
      //   Object.keys(this.tags).forEach((tagName) => {
      //     let tagValue = this.tags[tagName];
      //     // set tag value to string if not
      //     if (typeof tagValue !== 'string') {
      //       tagValue = JSON.stringify(tagValue);
      //     }
      //     if (scope.setTag) {
      //       scope.setTag(tagName, tagValue);
      //     }
      //   });
      //   if (this.req) {
      //     scope.setExtra('request url', this.req.url);
      //     scope.setExtra('request body', this.req.body);
      //     scope.setExtra('request params', this.req.params);
      //     scope.setExtra('request query', this.req.query);
      //   }
      //   if (this.errors) {
      //     scope.setExtra('original errors', this.errors);
      //   }
      // });

      // // actually not need req/user/tags
      // const eventId = await Sentry.captureException(this, {
      //   req: this.req,
      //   user: { id: this.req && this.req.body ? this.req.body.userId : undefined },
      //   tags: this.tags
      // });
      // if (eventId) {
      //   console.log('Captured exception and send to Sentry successfully');
      // } else {
      //   console.error('Failed to send captured exception to Sentry');
      // }
      // if (Sentry.flush) {
      //   await Sentry.flush(2000);
      // }
    } catch (err) {
      console.log('sentry logging err: ', err);
    }
  }
}

module.exports = apiError;
