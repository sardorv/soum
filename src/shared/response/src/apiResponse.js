const errCodeList = require('./errCodeList');

class apiResponse {
  /**
   *
   * @param {Object} data Data to send back, array or object
   * @param {number} [code=200] The HTTP status code to use
   * @param {string} [msgCode] The message code to send
   */
  constructor(data, code, msgCode) {
    // Set default code
    if (!code) {
      // eslint-disable-next-line no-param-reassign
      code = 200;
    }

    // Validate or set default msgCode
    if (msgCode) {
      // eslint-disable-next-line no-param-reassign
      msgCode = msgCode.toUpperCase();
      // eslint-disable-next-line no-param-reassign
      msgCode = msgCode.split(' ').join('_');
    } else {
      // eslint-disable-next-line no-param-reassign
      msgCode = errCodeList.codes.get(code) || 'UNKNOWN';
    }

    this.code = code;
    this.msgCode = msgCode;
    if(data) {
      this.data = data;      
    }

  }

  send(res) {
    const output = Object.assign({}, this);
    delete output.code;
    // eslint-disable-next-line no-restricted-properties
    res.status(this.code).send(output);
  }
}

module.exports = apiResponse;
