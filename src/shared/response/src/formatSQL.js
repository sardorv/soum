/**
 * Format a SQL ER_DUP_ENTRY as validation error
 * @param {*} error
 */
function formatSqlDupAsValidationError(error) {
  // get name of the duplicated field
  const field = error.sqlMessage.substring(
    error.sqlMessage.lastIndexOf(' for key \'') + ' for key \''.length,
    error.sqlMessage.lastIndexOf('\'')
  );

  // get value of duplicated field
  const value = error.sqlMessage.substring(
    error.sqlMessage.lastIndexOf(' entry \'') + ' entry \''.length,
    error.sqlMessage.lastIndexOf('\' for key ')
  );

  return [
    {
      field: [
        field
      ],
      messages: [
        `"${field}" must be a unique (${value})`
      ]
    }
  ];
}

module.exports = {
  formatSqlDupAsValidationError
};
