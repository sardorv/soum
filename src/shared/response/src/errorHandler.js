const expressValidation = require('express-validation');
const ApiError = require('./apiError');
const formatSql = require('./formatSQL');
const formatMongo = require('./formatMongo');
const formatValidation = require('./formatValidation');

// eslint-disable-next-line no-unused-vars
module.exports = (err, req, res, next) => {
  let localErr;

  if (err instanceof expressValidation.ValidationError || err.message === 'validation error') {
    /**
     * Handle express validation errors
     */
    localErr = new ApiError(400, 'VALIDATION_ERROR');
    localErr.setErrors(formatValidation.formatValidationErrors(err.errors));
    localErr.setStack(err.stack);
  } else if (err.name === 'ValidationError') {
    /**
     * Handle Express validation js
     */
    localErr = new ApiError(400, 'VALIDATION_ERROR');
    localErr.setErrors(formatValidation.formatExpressValidatonErrors(err.errors));
    localErr.setStack(err.stack);
  } else if (err.name === 'BulkWriteError' || err.name === 'MongoError') {
    console.log('err: ', err);

    /**
     * Handle Mongo errors
     */
    switch (err.code) {
      case 11000:
      case 11001:
        localErr = new ApiError(400, 'VALIDATION_ERROR');
        localErr.setErrors(formatMongo.formatMongoDupAsValidationError(err));
        localErr.setStack(err.stack);
        break;

      default:
        localErr = new ApiError();
        localErr.setErrors(err);
        localErr.setStack(err.stack);
        break;
    }
  } else if (err.name === 'PayloadTooLargeError') {
    console.log('err: ', err);

    localErr = new ApiError(413, 'REQUEST_ENTITY_TOO_LARGE');
  } else if (err.sql) {
    console.log('err: ', err);

    /**
     * Handle Mysql errors
     */
    switch (err.code) {
      case 'ER_DUP_ENTRY':
        localErr = new ApiError(400, 'VALIDATION_ERROR');
        localErr.setErrors(formatSql.formatSqlDupAsValidationError(err));
        localErr.setStack(err.stack);
        break;
      default:
        localErr = new ApiError();
        localErr.setErrors(err);
        localErr.setStack(err.stack);
        break;
    }
  } else if (!err.isApiError) {
    console.log('err: ', err);

    /**
     * Handle other error
     */
    localErr = new ApiError();
    localErr.setErrors(err);
    localErr.setStack(err.stack);
  } else {
    if (err.code === 500) {
      console.log('err: ', err);
    }
    localErr = err;
  }

  // localErr.output.payload.msgCode = localErr.data.msgCode;

  // if (localErr.isServer()) {
  // log the error...
  // probably you don't want to log unauthorized access
  // or do you?

  // Log system errors to CloudWatch
  // We need to log to cloudwatch and sentry the original message, not localErr

  // Setting user Id to senty error
  localErr.setReq(req);

  // Save to sentry
  localErr.log();
  // }

  // return res.status(localErr.code).json(localErr);
  return localErr.send(res);
};
