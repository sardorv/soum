# shared-response

Shared api response library for microservices

### Install

First, install the plugin:

```
> npm i -S @3i/response
```

### How to use it?

Library provides one middleware and a error class.

To use error handler middleware, do in app.js :

```
app.use(require('@shared/response').errorHandler);
```

To throw an error:

```
const ApiError = require('@shared/response').apiError;

throw new ApiError(404, 'TOUR_NOT_FOUND', 'This tour doesn\'t exist');
```

ApiError constructor takes 4 parameters:

- status : REQUIRED, http status code
- msgCode : REQUIRED, message code, used in front-end to display translated error message
- message : human readable error message
- options : boom construction options, see https://github.com/hapijs/boom#new-boommessage-options
