const express = require('express');
const swagger = require('./swagger');
const { graphqlHTTP } = require('express-graphql');
const graphQlAPP = require('./graphql/app');

const app = express();

app.use(
  '/graphql',
  graphqlHTTP({
    schema: graphQlAPP.schema,
    rootValue: graphQlAPP.root,
    graphiql: true,
  }),
);

app.use(swagger);
app.use(require('./middlewares/headers'));
app.use(require('./routes'));
app.use(require('./shared/response').errorHandler);

app.get('/status', (req, res) => {
  new ApiError(200, 'OK').send(res);
});
app.use((req, res) => {
  new ApiError(404, 'NOT_FOUND').send(res);
});

module.exports = app;
