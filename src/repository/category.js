const fs = require('fs');
const readData = async () => {
  path = __dirname + '/../../data/categories.json';
  const cat = JSON.parse(fs.readFileSync(path, 'utf8'));
  return cat;
};

const list = async (category_id = null) => {
  const data = await readData();
  if (category_id) {
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.category_id === category_id) {
        return element;
      }
    }
  }
  return data;
};

const listChildren = async parent_id => {
  const data = await readData();
  const children = [];
  if (parent_id) {
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.parent_id === parent_id) {
        children.push(element);
      }
    }
  }

  return children;
};

module.exports = {
  list: list,
  listChildren: listChildren,
};
