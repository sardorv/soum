const fs = require('fs');
const productDTO = require('../dto/product');

const readData = async () => {
  const path = __dirname + '/../../data/products.json';
  try {
    return JSON.parse(fs.readFileSync(path, 'utf8'));
  } catch (err) {
    console.error(err);
  }
};

const list = async params => {
  const data = await readData();
  if (params.id) {
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.id === params.id) {
        return element;
      }
    }
  }

  if (params.state) {
    const items = [];
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.state === params.state) {
        items.push(element);
      }
    }
    return items;
  }

  if (params.category_id) {
    const items = [];
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.category_id === params.category_id) {
        items.push(element);
      }
    }
    return items;
  }
  return data;
};

const writeChanges = async product => {
  try {
    let products = await readData();
    let updatedProduct;
    products = products.map(item => {
      if (item.id === product.id) {
        const temp = JSON.parse(JSON.stringify(item));
        temp.state = product.state.name;
        updatedProduct = temp;
        return temp;
      }
      return item;
    });
    path = __dirname + '/../../data/products.json';

    fs.writeFileSync(path, JSON.stringify(products));
    return updatedProduct;
  } catch (err) {
    console.error(err);
  }
};

const tranfer = async (product, newState) => {
  let p = new productDTO(product);
  switch (newState) {
    case 'draft':
      p.makeDraft();
      break;
    case 'available':
      p.makeAvailable();
      break;
    case 'reserved':
      p.reserve();
      break;
    case 'sold':
      p.sell();
      break;
    case 'returned':
      p.return();
      break;
    case 'deleted':
      p.delete();
      break;
    case 'expired':
      p.expire();
      break;
    case 'deletedDraft':
      p.makeDeletedDraft();
      break;
  }
  p = await writeChanges(p);
  return p;
};

module.exports = {
  list,
  tranfer,
};
