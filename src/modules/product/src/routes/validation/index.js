const Joi = require('joi');

module.exports = {
  isValidId: {
    params: {
      id: Joi.string()
        .regex(/^[0-9a-zA-Z-]{36}$/)
        .required(),
    },
  },
  transfer: {
    params: {
      id: Joi.string()
        .regex(/^[0-9a-zA-Z-]{36}$/)
        .required(),
    },
    body: {
      state: Joi.string()
        .valid(
          'draft',
          'available',
          'reserved',
          'sold',
          'returned',
          'deleted',
          'expired',
          'deletedDraft',
        )
        .required(),
    },
  },
};
