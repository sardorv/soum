const Product = require('@repository/product');
/* global formatPaginatedData */

const list = async (req, res, next) => {
  try {
    const query = {};
    if (req.query.state) {
      query.state = req.query.state;
    }
    const data = await Product.list(query);

    new ApiResponse(data, 200).send(res);
  } catch (err) {
    next(err);
  }
};

const load = async (req, res, next) => {
  try {
    const data = await Product.list({ id: req.params.id });
    if (!data) {
      throw new ApiError(404, 'PRODUCT_NOT_FOUND');
    }

    new ApiResponse(data, 200).send(res);
  } catch (err) {
    next(err);
  }
};

const transfer = async (req, res, next) => {
  try {
    const state = req.body.state;
    const product = await Product.list({ id: req.params.id });
    if (!product) {
      throw new ApiError(404, 'PRODUCT_NOT_FOUND');
    }

    const p = await Product.tranfer(product, state);

    new ApiResponse(p, 200).send(res);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  list,
  load,
  transfer,
};
