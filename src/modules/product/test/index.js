const chai = require('chai');
const chaiHttp = require('chai-http');
const test = require('../../../../test-server');
const { expect } = chai;
const should = chai.should();
const app = test.server;

const host = '/products';

chai.use(chaiHttp);
let products;

describe('Product module tests', () => {
  before(async () => {});
  it('List products', done => {
    chai
      .request(app)
      .get(`${host}/`)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.a('Object');
        res.body.msgCode.should.equal('OK');
        res.body.data.should.be.a('Array');
        products = res.body.data;
        done();
      });
  });

  it('Load a Product', done => {
    chai
      .request(app)
      .get(`${host}/${products[0].id}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.a('Object');
        res.body.msgCode.should.equal('OK');
        res.body.data.should.be.a('Object');
        res.body.data.should.have.property('name');
        done();
      });
  });

  it('Trasfer a Product: available -> reserved', done => {
    const draft = products.find(o => o.state === 'available');
    if (!draft) done();
    const data = {
      state: 'reserved',
    };
    chai
      .request(app)
      .patch(`${host}/${draft.id}/transfer`)
      .send(data)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.a('Object');
        res.body.msgCode.should.equal('OK');
        res.body.data.should.be.a('Object');
        res.body.data.should.have.property('name');
        done();
      });
  });

  it('Trasfer a Product: draft -> sold', done => {
    const draft = products.find(o => o.state === 'draft');
    if (!draft) done();
    const data = {
      state: 'sold',
    };
    chai
      .request(app)
      .patch(`${host}/${draft.id}/transfer`)
      .send(data)
      .end((err, res) => {
        res.should.have.status(409);
        res.should.be.a('Object');
        res.body.msgCode.should.equal(
          'OPERATION_IS_NOT_POSSIBLE_FOR_DRAFT_PRODUCT',
        );
        done();
      });
  });
});
