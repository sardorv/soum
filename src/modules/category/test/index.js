const chai = require('chai');
const chaiHttp = require('chai-http');
const test = require('../../../../test-server');
const { expect } = chai;
const should = chai.should();
const app = test.server;

const host = '/categories';

chai.use(chaiHttp);
let categories;

describe('Category module tests', () => {
  before(async () => {});
  it('List categories', done => {
    chai
      .request(app)
      .get(`${host}/`)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.a('Object');
        res.body.msgCode.should.equal('OK');
        res.body.data.should.be.a('Array');
        categories = res.body.data;
        done();
      });
  });

  it('Load a category', done => {
    chai
      .request(app)
      .get(`${host}/${categories[0].category_id}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.a('Object');
        res.body.msgCode.should.equal('OK');
        res.body.data.should.be.a('Object');
        res.body.data.should.have.property('category_name');
        done();
      });
  });
});
