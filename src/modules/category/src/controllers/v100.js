const Category = require('@repository/category');
/* global formatPaginatedData */

const list = async (req, res, next) => {
  try {
    const data = await Category.list();

    new ApiResponse(data, 200).send(res);
  } catch (err) {
    next(err);
  }
};

const load = async (req, res, next) => {
  try {
    const data = await Category.list(req.params.id);
    if (!data) {
      throw new ApiError(404, 'CATEGORY_NOT_FOUND');
    }

    new ApiResponse(data, 200).send(res);
  } catch (err) {
    next(err);
  }
};

const listChildren = async (req, res, next) => {
  try {
    const category = await Category.list(req.params.id);
    if (!category) {
      throw new ApiError(404, 'CATEGORY_NOT_FOUND');
    }
    const data = await Category.listChildren(req.params.id);

    new ApiResponse(data, 200).send(res);
  } catch (err) {
    next(err);
  }
};

module.exports = {
  list,
  load,
  listChildren,
};
