const Joi = require('joi');

module.exports = {
  isValidId: {
    params: {
      id: Joi.string()
        .regex(/^[0-9a-zA-Z-]{36}$/)
        .required(),
    },
  },
};
