const validate = require('express-validation');
const express = require('express');
const validations = require('./validation');
const apiByVersion = require('../middlewares/apiByVersion');
const ctrl = require('../controllers');

const router = express.Router();

router.route('/').get(apiByVersion(ctrl, 'list'));
router
  .route('/:id')
  .get(validate(validations.isValidId), apiByVersion(ctrl, 'load'));
router
  .route('/:id/children')
  .get(validate(validations.isValidId), apiByVersion(ctrl, 'listChildren'));

module.exports = router;
