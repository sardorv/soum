const fs = require('fs');

class Product {
  constructor(p) {
    this.id = p.id;
    this.name = p.name;
    this.price = p.price;
    this.images = p.images;
    this.category_id = p.category_id;
    this.state = this.setState(p.state);
  }
  changeState(state) {
    this.state = state;
  }

  setState(state) {
    switch (state) {
      case 'draft':
        return new Draft(this);
      case 'available':
        return new Available(this);
      case 'reserved':
        return new Reserved(this);
      case 'sold':
        return new Sold(this);
      case 'returned':
        return new Returned(this);
      case 'deleted':
        return new Deleted(this);
      case 'expired':
        return new Expired(this);
      case 'deletedDraft':
        return new DeletedDraft(this);
    }
  }

  runMethod(method) {
    if (typeof this.state[method] === 'undefined') {
      throw new ApiError(
        409,
        'Operation is not possible for ' + this.state.name + ' product',
      );
      // throw new Error(
      //   'Operation is not possible for ' + this.state.name + ' product',
      // );
    }
    this.state[method]();
  }

  makeDraft() {
    this.runMethod('makeDraft');
  }
  makeAvailable() {
    this.runMethod('makeAvailable');
  }
  reserve() {
    this.runMethod('reserve');
  }
  sell() {
    this.runMethod('sell');
  }
  return() {
    this.runMethod('return');
  }
  delete() {
    this.runMethod('delete');
  }
  expire() {
    this.runMethod('expire');
  }
  makeDeletedDraft() {
    this.runMethod('makeDeletedDraft');
  }
}

class Draft {
  constructor(product) {
    this.name = 'draft';
    this.product = product;
  }
  makeAvailable() {
    this.product.changeState(new Available(this.product));
  }
  makeDeletedDraft() {
    this.product.changeState(new DeletedDraft(this.product));
  }
}

class Available {
  constructor(product) {
    this.name = 'available';
    this.product = product;
  }
  delete() {
    this.product.changeState(new Deleted(this.product));
  }
  reserve() {
    this.product.changeState(new Reserved(this.product));
  }
  expire() {
    this.product.changeState(new Expired(this.product));
  }
}

class Reserved {
  constructor(product) {
    this.name = 'reserved';
    this.product = product;
  }
  makeAvailable() {
    this.product.changeState(new Available(this.product));
  }
  sell() {
    this.product.changeState(new Sold(this.product));
  }
}

class Sold {
  constructor(product) {
    this.name = 'sold';
    this.product = product;
  }
  return() {
    this.product.changeState(new Returned(this.product));
  }
}

class Returned {
  constructor(product) {
    this.name = 'returned';
    this.product = product;
  }
  makeDraft() {
    this.product.changeState(new Draft(this.product));
  }
  delete() {
    this.product.changeState(new Deleted(this.product));
  }
}

class Deleted {
  constructor(product) {
    this.name = 'deleted';
    this.product = product;
  }
}

class Expired {
  constructor(product) {
    this.name = 'expired';
    this.product = product;
  }
  makeAvailable() {
    this.product.changeState(new Available(this.product));
  }
}

class DeletedDraft {
  constructor(product) {
    this.name = 'deletedDraft';
    this.product = product;
  }
}

module.exports = Product;
