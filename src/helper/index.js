const { ApiError, ApiResponse } = require('../shared/response');

global.ApiError = ApiError;
global.ApiResponse = ApiResponse;

/* global isLocal:true */
isLocal = () => ['local', 'test'].indexOf(process.env.NODE_ENV) >= 0;

/* global isTest:true */
isTest = () => process.env.NODE_ENV === 'test';

module.exports = {
  isLocal,
  isTest,
};
