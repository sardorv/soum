const router = require('express').Router();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerOptions = {
  swaggerDefinition: {
    openapi: '3.0.0',
    servers: [
      {
        url: process.env.APP_HOST,
        description: 'Development server',
      },
    ],
    info: {
      title: 'SOUM API',
      description: 'SOUM API Information',
    },
  },
  apis: [
    `${basePath}/src/swagger/docs/*.yaml`,
    `${basePath}/src/modules/**/docs/*.yaml`,
  ],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

module.exports = router;
