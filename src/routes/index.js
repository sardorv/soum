const express = require('express');

const router = express.Router();

router.use(express.json());

router.use('/products', require('@root/modules/product/src/routes'));
router.use('/categories', require('@root/modules/category/src/routes'));

module.exports = router;
