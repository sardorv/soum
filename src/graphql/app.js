const { buildSchema } = require('graphql');
const fs = require('fs');

// Initialize a GraphQL schema
var schema = buildSchema(`
  type Query {
    category(category_id: String): Category
    categories(parent_id: String): [Category]
    product(id: String): Product
    products(state: String): [Product]
  },
  type Category {
    category_id: String
    category_name: String
    parent_id: String
  }
  type Product {
    id: String
    name: String
    price: String
    state: String
  }
`);

const loadCategory = args => {
  var category_id = args.category_id;
  const path = __dirname + '/../../data/categories.json';
  const categories = JSON.parse(fs.readFileSync(path, 'utf8'));
  return categories.filter(category => category.category_id == category_id)[0];
};

const listCategories = args => {
  const path = __dirname + '/../../data/categories.json';
  const categories = JSON.parse(fs.readFileSync(path, 'utf8'));
  if (args.parent_id) {
    var parent_id = args.parent_id;
    return categories.filter(category => category.parent_id === parent_id);
  } else {
    return categories;
  }
};

const loadProduct = args => {
  var id = args.id;
  const path = __dirname + '/../../data/products.json';
  const products = JSON.parse(fs.readFileSync(path, 'utf8'));
  return products.filter(product => product.id == id)[0];
};

const listProducts = args => {
  const path = __dirname + '/../../data/products.json';
  const products = JSON.parse(fs.readFileSync(path, 'utf8'));
  if (args.state) {
    var state = args.state;
    return products.filter(product => product.state === state);
  } else {
    return products;
  }
};

// Root resolver
var root = {
  category: loadCategory, // Resolver function to return user with specific id
  categories: listCategories,
  product: loadProduct,
  products: listProducts,
};

module.exports = {
  schema,
  root,
};
