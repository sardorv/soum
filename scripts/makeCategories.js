const fs = require('fs');
const uuid = require('uuid');
const faker = require('faker');

const commerce = faker.commerce;

const getParentId = categories => {
  if (categories.length < 1) {
    return -1;
  }
  let item = null;
  for (let i = 0; i < 2; i++) {
    item = categories[Math.floor(Math.random() * categories.length)];
    if (!item.parent_id) {
      return item.category_id;
    }
  }
  return -1;
};

const genCategories = limit => {
  const categories = [];
  const catMap = {};
  for (let i = 0; i < limit; i++) {
    const item = {
      category_id: uuid(),
      category_name: faker.commerce.department(),
    };
    if (catMap[item.category_name]) {
      i--;
      continue;
    }
    const parent_id = getParentId(categories);
    if (parent_id != -1) {
      item.parent_id = parent_id;
    }
    categories.push(item);
    catMap[item.category_name] = 1;
  }
  return categories;
};

const makeCategories = limit => {
  const categories = genCategories(limit);
  path = __dirname + '/../data/categories.json';
  try {
    fs.writeFileSync(path, JSON.stringify(categories));
  } catch (err) {
    console.error(err);
  }
};

makeCategories(10);
