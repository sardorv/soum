const fs = require('fs');
const uuid = require('uuid');
const faker = require('faker');

const PRODUCT_STATES = [
  'draft',
  'available',
  'reserved',
  'sold',
  'returned',
  'deleted',
  'expired',
  'deletedDraft',
];

const getCategoryId = () => {
  path = __dirname + '/../data/categories.json';
  try {
    const categories = JSON.parse(fs.readFileSync(path, 'utf8'));
    const item = categories[Math.floor(Math.random() * categories.length)];
    return item.category_id;
  } catch (err) {
    console.error(err);
  }
};

const genProducts = limit => {
  const products = [];
  const productMap = {};
  for (let i = 0; i < limit; i++) {
    const item = {
      id: uuid(),
      name: faker.commerce.productName(),
      price: faker.commerce.price(100, 200, 0, '$'),
      images: [],
    };
    // Generate 4 random images for each product
    for (let j = 0; j < 4; j++) {
      const image = {
        url: faker.image.imageUrl(640, 480, 'cat', true, true),
      };
      item.images.push(image);
    }
    // Randomly choose main image
    const randIndex = Math.floor(Math.random() * (3 - 0 + 1) + 0);
    item.images[randIndex]['main'] = true;

    // Randomly choose product state
    item.state =
      PRODUCT_STATES[Math.floor(Math.random() * PRODUCT_STATES.length)];
    // If faker generates same product name; Regenerate
    if (productMap[item.name]) {
      i--;
      continue;
    }
    item.category_id = getCategoryId();
    products.push(item);
    productMap[item.name] = 1;
  }
  return products;
};

const makeProducts = limit => {
  const products = genProducts(limit);
  path = __dirname + '/../data/products.json';
  try {
    fs.writeFileSync(path, JSON.stringify(products));
  } catch (err) {
    console.error(err);
  }
};

makeProducts(100);
