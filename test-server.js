/* eslint-disable import/no-extraneous-dependencies */
const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('./server');

chai.use(chaiHttp);

const genRandomName = (name = '') =>
  `${name}_${Math.random().toString(36).substring(5)}`;

module.exports = {
  server,
  genRandomName,
};
