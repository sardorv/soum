global.basePath = __dirname;
global.storagePath = `${global.basePath}/../static/storage`;
const dotenv = require('dotenv');
const fs = require('fs');

if (fs.existsSync(`${__dirname}/.env`)) {
  dotenv.config({ path: `${__dirname}/.env` });
}
if (fs.existsSync(`${__dirname}/.env.${process.env.NODE_ENV}`)) {
  dotenv.config({ path: `${__dirname}/.env.${process.env.NODE_ENV}` });
}
require('module-alias/register');
require('./src/helper');
const app = require('./src/app');

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server Running at http://127.0.0.1:${port}`);
});

module.exports = app;
